# bkp

**bkp** is a bash shell script which aims to simplify backing up your
system with rsync. Note: it is tailored to my specific purposes, but might
be instructive and useful for others.

## My Setup

I have a desktop machine called *yosemite* which has two 500G removable
SSDs in it, labeled "a" and "b". Yosemite has a 250M M.2 drive which holds
the root partition, and a 500G internal SSD which holds the `/home`
partition. I have a script called "backup" at `/etc/cron.daily` which
controls all the backups. The backup script uses the **bkp** program to
backup the root filesystem, then the home partition to the "a" backup
drive. Then it repeats this process for the "b" backup drive.

There is a file `/etc/backup-rootfs` which controls the parameters for the
root filesystem backup, and a file `/etc/backup-home` which controls the
parameters for the home backup. In additions there are two files which
specify files/directories to *exclude* from backups: `/etc/excludes-rootfs`
and `/etc/excludes-home`.

## Bkp Operation

**Bkp** isn't run interactively. You can provide command line parameters and/or
a config file you can fill with parameters. `bkp` can run three types of
backup. First, it can run a root filesystem backup, specified on the
command line as `--rootfs`. This backs up the root filesystem (not
including the home partition), and excludes certain files and directories,
like `/proc` and `/tmp`. The second type of backup is the home partition,
designated as `--home` on the command line. This, too, excludes some files
and directories. The third type of backup is the "mirror" backup, specified
as `--mirror` on the command line. This just copies the files from the "a"
backup drive to the "b" backup drive.

For **home** and **rootfs** backups, `bkp` uses "snapshot" backups. In my
case, I specify 7 snapshots. This means that when a current backup is made,
the prior backup is moved to a different directory to provide yesterday's
version of the backup. If this were done by conventional means, 7 days'
worth of backups would take up 7 times the room of a single backup.
Instead, files which are duplicates in the prior backup are simply hard
linked (see `ln(1)`) to the current files. This means that the room all the
backups take up consists of the space taken up by today's backup, and some
extra for each file which was changed in the last 7 days. This means my
"staged" backups take up far less room than they normally would. This
scheme is based on the following article:
http://www.mikerubel.org/computers/rsync_snapshots/ .

**Bkp** echoes the results of its operation to a log file which is dumped
on the backup drive for later viewing. This gives details of what was done
to each file, and a summary of the backup. When run as a cron job, this
also appears in an email which is sent out after the operation is complete.

**Bkp** does not encrypt backups. All files are usable exactly as they
originally were, and links, permissions and ownership are preserved.

All config files are included in the distribution, along with a *man* page.

## License

**bkp** is provided without warranty, and with no claims of its fitness for
any purpose. Moreover, the author assumes no liability for any loss of data
or damage to your computer or attached devices. Your use of this software
is at your own risk.

**bkp** is provided under a GPLv2 license.

## Author

This software is written and copyright by Paul M. Foster
<paulf@quillandmouse.com>.
