install:
	cp bkp /usr/local/bin
	cp backup-home /etc
	cp backup-rootfs /etc
	cp backup-mirror /etc
	cp excludes-home /etc
	cp excludes-rootfs /etc
	cp backup /etc/cron.daily
	cp bkp.1 /usr/local/man/man1
